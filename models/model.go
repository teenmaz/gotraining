package model

import (
	"g.ghn.vn/go-training/tientp/libs/mongoDB"
	"g.ghn.vn/go-training/tientp/types"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Store define a SQL mapper
type Store struct {
}

func (m *Store) getDB() *mgo.Collection {
	mongo := mongoDB.GetInstance("default")
	collection := mongo.GetCollection("users")
	return collection
}

// NewStore instance
func NewStore() Store {
	return Store{}
}

// FindAll users in database
func (m *Store) FindAll() ([]types.Profile, error) {
	var users []types.Profile
	m.getDB().Find(bson.M{}).All(&users)
	return users, nil
}

// GetOne
func (m *Store) GetOne(id int) (types.Profile, error) {
	var item types.Profile

	err := m.getDB().FindId(id).One(&item)

	return item, err
}

// GetOne by Token
func (m *Store) GetOneByToken(token string) (types.Profile, error) {
	var item types.Profile
	err := m.getDB().Find(bson.M{"token": token}).One(&item)
	return item, err
}

func (m *Store) GetOneByField(field, value string) (types.Profile, error) {
	var item types.Profile
	err := m.getDB().Find(bson.M{string(field): value}).One(&item)
	return item, err
}

// Insert implement Mapper interface
func (m *Store) Insert(p *types.Profile) error {
	return m.getDB().Insert(p)
}

// Delete implement Mapper interface
func (m *Store) Delete(id int) error {
	return m.getDB().Remove(bson.M{"_id": id})
}
