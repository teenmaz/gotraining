package model

import (
	"fmt"

	"g.ghn.vn/go-training/tientp/libs/mongoDB"
	"g.ghn.vn/go-training/tientp/types"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// ListAds define a SQL mapper
type ListAds struct {
}

func (m *ListAds) getDB() *mgo.Collection {
	mongo := mongoDB.GetInstance("default")
	collection := mongo.GetCollection("ListAds")
	return collection
}

// NewStore instance
func NewListAds() ListAds {
	return ListAds{}
}

// FindAll users in database
func (m *ListAds) FindAll(limit int) ([]types.ChototListAds, error) {
	var chototListAds []types.ChototListAds
	m.getDB().Find(bson.M{}).Limit(limit).All(&chototListAds)
	return chototListAds, nil
}

// GetOne
func (m *ListAds) GetOne(id int) (types.Profile, error) {
	var item types.Profile

	err := m.getDB().FindId(id).One(&item)

	return item, err
}

// GetOne by Token
func (m *ListAds) GetOneByToken(token string) (types.Profile, error) {
	var item types.Profile
	err := m.getDB().Find(bson.M{"token": token}).One(&item)
	return item, err
}

func (m *ListAds) GetOneByField(field, value string) (types.Profile, error) {
	var item types.Profile
	err := m.getDB().Find(bson.M{string(field): value}).One(&item)
	return item, err
}

// Insert implement Mapper interface
func (m *ListAds) Insert(p *types.ChototListAds) error {
	return m.getDB().Insert(p)
}

func (m *ListAds) FindLastOne() (types.ChototListAds, error) {
	var item types.ChototListAds

	info, err := m.getDB().RemoveAll(bson.M{"ads": bson.M{"$gt": "[]"}})

	fmt.Println("Change:", info)

	err = m.getDB().Find(bson.M{}).Sort("-$natural").One(&item)

	return item, err
}

// Delete implement Mapper interface
func (m *ListAds) Delete(id int) error {
	return m.getDB().Remove(bson.M{"_id": id})
}
