package logger

import (
	"go.uber.org/zap"
)

func GetLogger(module string) *zap.SugaredLogger {
	//log, _ := zap.NewDevelopment()
	log, _ := zap.NewProduction()

	if module == "" {
		return log.Sugar()
	}
	return log.Sugar().With("module", module)
}
