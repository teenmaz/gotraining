package cache

import (
	"encoding/json"
	"fmt"
	"time"

	"g.ghn.vn/go-training/tientp/config"
	"g.ghn.vn/go-training/tientp/logger"
	model "g.ghn.vn/go-training/tientp/models"
	"g.ghn.vn/go-training/tientp/types"
	"github.com/go-redis/redis"
	"golang.org/x/crypto/bcrypt"
)

var (
	rc    *redis.Client
	mongo *model.Store
)
var log = logger.GetLogger("Cache")

const (
	profileKey = "profile:%d:data"
)

//Init redis client
func init() {
	rc = redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%d", config.Config.Redis.Host, config.Config.Redis.Port),
		DB:   config.Config.Redis.Database, // use default DB
	})
	mongo = &model.Store{}

	fmt.Println(mongo)
}

func GetProfileByAccountID(accountID int) *types.Profile {
	profileData, err := rc.Get(fmt.Sprintf(profileKey, accountID)).Result()

	if err != nil {
		log.Debug("Failed getting from cache:", err)
		return nil
	}
	var data types.Profile
	err = json.Unmarshal([]byte(profileData), &data)

	if err != nil {
		log.Debugf("Failed marshaling from cache:%s, %+v", err, profileData)
		return nil
	}

	return &data
}

func SetProfileByAccountID(accountID int, profile *types.Profile) {
	if profile == nil {
		return
	}
	bProfile, err := json.Marshal(profile)
	if err != nil {
		log.Error("Error marshal  profile ", profile)
	}
	rc.Set(fmt.Sprintf(profileKey, accountID), bProfile, time.Duration(config.Config.Redis.TTL)*time.Second)
}

func CreateNewUser(profile *types.ProfileRequest) error {
	var InProfile types.Profile

	bProfile, err := json.Marshal(profile)

	err = json.Unmarshal(bProfile, &InProfile)

	hash, err := bcrypt.GenerateFromPassword([]byte(InProfile.PassWord), bcrypt.DefaultCost)
	if err != nil {
		// TODO: Properly handle error
		log.Fatal(err)
	}
	InProfile.PassWord = string(hash)
	fmt.Println("profile:", InProfile)
	if err != nil {
		log.Error("Error marshal  profile ", profile)
	}
	return mongo.Insert(&InProfile)
}

func DelProfileByAccountID(accountID int) {
	status := rc.Del(fmt.Sprintf(profileKey, accountID))
	log.Info(status)
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func GetUserByEmail(email, password string) (types.Profile, bool) {
	profile, err := mongo.GetOneByField("email", email)
	if err != nil {
		log.Error("Error marshal  profile ", err)
		return types.Profile{}, false
	}
	hash := CheckPasswordHash(password, profile.PassWord)
	return profile, hash

}
