# Golang training with echo framework

[![N|Solid](https://giaohangnhanh.vn/wp-content/themes/giaohangnhanh/images/logo_giaohangnhanh.png)](https://g.ghn.vn/go-training/echo-demo)

The Go programming language is an open source project to make programmers more productive.

Go is expressive, concise, clean, and efficient. Its concurrency mechanisms make it easy to write programs that get the most out of multicore and networked machines, while its novel type system enables flexible and modular program construction. Go compiles quickly to machine code yet has the convenience of garbage collection and the power of run-time reflection. It's a fast, statically typed, compiled language that feels like a dynamically typed, interpreted language.

# Installing Go

  Once connected, update and upgrade the Ubuntu packages on your server. This ensures that you have the latest security patches and fixes, as well as updated repos for your new packages.
  ```sh
$ sudo apt-get update
$ sudo apt-get -y upgrade
 ```
With that complete, you can begin downloading the latest package for Go by running this command, which will pull down the Go package file, and save it to your current working directory, which you can determine by running ```pwd```.
```sh
$ sudo curl -O https://storage.googleapis.com/golang/go1.9.linux-amd64.tar.gz
```
 Next, use tar to unpack the package. This command will use the Tar tool to open and expand the downloaded file, and creates a folder using the package name, and then moves it to ```/usr/local```.
```sh
$ sudo tar -xvf go1.9.linux-amd64.tar.gz
$ sudo mv go /usr/local
```
# Setting Go Paths

First, set Go's root value, which tells Go where to look for its files.
```sh
$ sudo nano ~/.profile
```
At the end of the file, add this line:

```sh 
export PATH=$PATH:/usr/local/go/bin 
```

If you chose an alternate installation location for Go, add these lines instead to the same file. This example shows the commands if Go is installed in your home directory:
```sh
export GOROOT=$HOME/go
export PATH=$PATH:$GOROOT/bin
```
# Setup Go workspace
Create a new directory for your Go workspace, which is where Go will build its files.
```sh
$ mkdir $HOME/work
```
Now you can point Go to the new workspace you just created by exporting ```GOPATH```.

```sh 
$ export GOPATH=$HOME/work
```

# Clone echo-demo project on g.ghn.vn and run server
Create a directory hierarchy in this folder through this command in order for you to create your project
```sh
$ cd $GOPATH
$ mkdir -p src/g.ghn.vn/go-training
$ cd src/g.ghn.vn/go-training
$ git clone git@g.ghn.vn:go-training/echo-demo.git
$ cd echo-demo
$ go get
$ go run main.go
```

<pre>
POSTMAN:
https://www.getpostman.com/collections/6f8c99dd73bbf0642cbe
</pre>