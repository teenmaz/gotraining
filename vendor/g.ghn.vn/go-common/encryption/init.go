package encryption

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/hex"
	"fmt"
)

var defaultKey = []byte("ghn0123456789")
var defaultIV = []byte("\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f")

type AESEncryptionOption func(*AESEncryption) error

type AESEncryption struct {
	key   []byte
	iv    []byte
	block cipher.Block
}

func NewAesCryption(options ...AESEncryptionOption) *AESEncryption {
	c := &AESEncryption{
		key: defaultKey,
		iv:  defaultIV,
	}
	for _, option := range options {
		if err := option(c); err != nil {
			return nil
		}
	}

	block, err := aes.NewCipher(c.key)
	if err != nil {
		panic(err)
	}
	c.block = block
	return c
}

func SetAESKey(key string) AESEncryptionOption {
	return func(c *AESEncryption) error {
		c.key = []byte(key)
		return nil
	}
}

func SetAESIV(iv string) AESEncryptionOption {
	return func(c *AESEncryption) error {
		c.iv = []byte(iv)
		return nil
	}
}

func ToByteArrayWithPad(text string) []byte {
	length := (aes.BlockSize - (len(text) % aes.BlockSize))
	buf := bytes.NewBufferString(text)
	for i := 0; i < length; i++ {
		buf.WriteByte(byte(length))
	}
	return buf.Bytes()
}

func ToByteArrayWithUnPad(textByte []byte) []byte {
	if len(textByte) == 0 {
		return nil
	}
	padding := textByte[len(textByte)-1]
	if int(padding) > len(textByte) || padding > aes.BlockSize {
		return nil
	} else if padding == 0 {
		return nil
	}
	for i := len(textByte) - 1; i > len(textByte)-int(padding)-1; i-- {
		if textByte[i] != padding {
			return nil
		}
	}
	return textByte[:len(textByte)-int(padding)]
}

func (c *AESEncryption) Encrypt(text string) string {
	byteText := ToByteArrayWithPad(text)

	ciphertext := make([]byte, len(byteText))
	mode := cipher.NewCBCEncrypter(c.block, c.iv)
	mode.CryptBlocks(ciphertext, byteText)
	return fmt.Sprintf("%x", ciphertext)
}

func (c *AESEncryption) Decrypt(text string) string {
	ciphertext, _ := hex.DecodeString(text)

	if len(ciphertext) < aes.BlockSize {
		panic("ciphertext too short " + text)
	}
	stream := cipher.NewCBCDecrypter(c.block, c.iv)
	stream.CryptBlocks(ciphertext, ciphertext)
	ciphertext = ToByteArrayWithUnPad(ciphertext)
	return fmt.Sprintf("%s", ciphertext)
}
