package config

import (
	"fmt"
	"strings"

	"github.com/spf13/viper"
)

type Schema struct {
	Profiler struct {
		Prometheus    bool   `mapstructure:"prometheus"`
		StatsdAddress string `mapstructure:"statsd_address"`
		Service       string `mapstructure:"service"`
	} `mapstructure:"profiler"`

	Redis struct {
		Host     string `mapstructure:"host"`
		Port     int    `mapstructure:"port"`
		Database int    `mapstructure:"database"`
		TTL      int    `mapstructure:"ttl"`
	} `mapstructure:"redis"`

	Mongo map[string]struct {
		Host string `mapstructure:"host"`
		Port int    `mapstructure:"port"`
		Name string `mapstructure:"name"`
		User string `mapstructure:"user"`
		Pass string `mapstructure:"pass"`
	} `mapstructure:"mongo"`

	Encryption struct {
		OIDKey    string `mapstructure:"oid_key"`
		SecretKey string `mapstructure:"secret_key"`
	} `mapstructure:"encryption"`
}

var Config Schema

func init() {
	config := viper.New()
	config.SetConfigName("echodemo")
	config.AddConfigPath(".")          // Look for config in current directory
	config.AddConfigPath("config/")    // Optionally look for config in the working directory.
	config.AddConfigPath("../config/") // Look for config needed for tests.
	config.AddConfigPath("../")        // Look for config needed for tests.

	config.SetEnvKeyReplacer(strings.NewReplacer(".", "__"))
	config.AutomaticEnv()

	err := config.ReadInConfig() // Find and read the config file
	if err != nil {              // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	err = config.Unmarshal(&Config)
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	fmt.Printf("Current Config: %+v", Config)
}
