distname=echo-demo
package=g.ghn.vn/go-training/echo-demo
goversion=1.8.0

build:
	docker run -v `pwd`:/go/src/$(package) golang:$(goversion)-alpine go build -o /go/src/$(package)/dist/$(distname) -v $(package)
