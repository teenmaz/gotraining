package handlers

import (
	"fmt"
	"net/http"
	"time"

	"g.ghn.vn/go-training/tientp/cache"
	"g.ghn.vn/go-training/tientp/config"
	"g.ghn.vn/go-training/tientp/types"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/liip/sheriff"
)

var notFoundErrorMessage = types.PayloadResponse("404", "Không tìm thấy thông tin người dùng")

var optionPublic = &sheriff.Options{
	Groups: []string{"public"},
}

func init() {}

func GetProfile(c echo.Context) error {
	accountID, _ := parseAccountID(c.Param("account_id"))
	log.Debug("AccountId: ", accountID)

	profile := getProfileByAccountID(accountID)
	if profile == nil {
		return c.JSON(http.StatusNotFound, notFoundErrorMessage)
	}
	log.Debug(profile)

	data, err := sheriff.Marshal(optionPublic, profile)
	if err != nil {
		log.Error(err)
	}
	return c.JSON(http.StatusOK, data)
}

func Register(c echo.Context) error {
	//var objProfileRequest types.ProfileRequest
	u := new(types.ProfileRequest)
	if err := c.Bind(u); err != nil {
		log.Errorf("Wrong request %s", err)
		return c.JSON(http.StatusBadRequest, types.PayloadResponse("payload_wrong", "Có lỗi xảy ra, vui lòng thử lại"))
	}
	if err := c.Validate(u); err != nil {
		//log.Errorf("Wrong request %s", err)
		return c.JSON(http.StatusBadRequest, types.PayloadResponse("wrong_request", fmt.Sprintf("%s", err)))
	}
	if *u.FullName == "" {
		return c.JSON(http.StatusBadRequest, types.PayloadResponse("wrong_request", "Key: 'ProfileRequest.FullName' Error:Field validation for 'FullName' failed on the 'required' tag"))
	}
	err := cache.CreateNewUser(u)
	if err != nil {
		log.Error(err)
	}
	res := types.OkStatus{
		Ok: true,
	}
	return c.JSON(http.StatusOK, res)
}

func Login(c echo.Context) error {
	u := new(types.LoginRequest)
	if err := c.Bind(u); err != nil {
		log.Errorf("Wrong request %s", err)
		return c.JSON(http.StatusBadRequest, types.PayloadResponse("payload_wrong", "Có lỗi xảy ra, vui lòng thử lại"))
	}
	if err := c.Validate(u); err != nil {
		//log.Errorf("Wrong request %s", err)
		return c.JSON(http.StatusBadRequest, types.PayloadResponse("wrong_request", fmt.Sprintf("%s", err)))
	}
	profile, hash := cache.GetUserByEmail(*u.Email, *u.PassWord)
	if hash == false {
		return c.JSON(http.StatusBadRequest, types.PayloadResponse("request_wrong", "Sai mật khẩu hoặc email"))
	}
	token := jwt.New(jwt.SigningMethodHS256)
	// Set claims
	claims := token.Claims.(jwt.MapClaims)

	accountid := profile.AccountID.Hex()

	claims["full_name"] = profile.FullName
	claims["pol"] = "59a795909e65177c27fcdd87"
	claims["type"] = "Admin"
	claims["sub"] = "Unknown mean"
	claims["Email"] = profile.Email
	claims["_id"] = string(accountid)
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()
	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte(config.Config.Encryption.SecretKey))
	if err != nil {
		return c.JSON(http.StatusInternalServerError, types.PayloadResponse("request_wrong", fmt.Sprintf("%s", err)))
	}
	return c.JSON(http.StatusOK, map[string]string{
		"token": t,
	})
	return echo.ErrUnauthorized
}
