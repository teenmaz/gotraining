package handlers

import (
	"strconv"

	"g.ghn.vn/go-common/encryption"
	"g.ghn.vn/go-training/tientp/cache"
	"g.ghn.vn/go-training/tientp/config"
	"g.ghn.vn/go-training/tientp/types"
)

var encryptionAlgo = encryption.NewAesCryption(encryption.SetAESKey(config.Config.Encryption.OIDKey))

func parseAccountID(accountIDString string) (int, bool) {
	isHash := false
	if len(accountIDString) > 16 {
		accountIDString = encryptionAlgo.Decrypt(accountIDString)
		isHash = true
	}
	accountID, err := strconv.Atoi(accountIDString)
	if err != nil {
		log.Errorf("Get account id error: %s", err)
	}
	return accountID, isHash
}

func getProfileByAccountID(accountID int) *types.Profile {
	profile := cache.GetProfileByAccountID(accountID)
	//profile.AccountOID = encryptionAlgo.Encrypt(strconv.Itoa(profile.AccountID))
	return profile

}
