package handlers

import (
	"net/http"

	"g.ghn.vn/go-training/tientp/logger"
	. "g.ghn.vn/go-training/tientp/types"
	"github.com/labstack/echo"
)

var log = logger.GetLogger("End-point")

func init() {
	// Init database.
	log.Info("Initializing database\n")
}
func HealthCheck(c echo.Context) error {
	log.Info("Health check ok")
	res := &OkStatus{
		Ok: true,
	}
	return c.JSON(http.StatusOK, res)
}
