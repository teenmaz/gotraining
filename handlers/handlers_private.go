package handlers

import (
	"net/http"

	"g.ghn.vn/go-training/tientp/types"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/liip/sheriff"
)

var optionPrivate = &sheriff.Options{
	Groups: []string{"private"},
}

func GetPrivateProfile(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(*types.JwtTPTClaims)
	return c.JSON(http.StatusOK, claims)
	//return c.String(http.StatusOK, fmt.Sprintf("Hello %s! - Email:%s", name, email))
}
