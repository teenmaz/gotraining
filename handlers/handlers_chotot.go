package handlers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	model "g.ghn.vn/go-training/tientp/models"
	"g.ghn.vn/go-training/tientp/types"
	"github.com/labstack/echo"
)

/* ProxyUrl,_ := url.Parse("http://localhost:8123") */

var ChoTotClient = &http.Client{Timeout: 10 * time.Second}

var LinkListAds string = "https://gateway.chotot.com/v1/public/ad-listing?region=13&page=%v"

var LinkDetailAds string = "https://gateway.chotot.com/v1/public/ad-listing/%v"

var mongo = &model.ListAds{}

func init() {
	/* LinkListAds = fmt.Sprintf(LinkListAds, 0) */
	/* proxyString := "http://localhost:8123"
	proxyUrl, _ := url.Parse(proxyString)
	tr := &http.Transport{
		Proxy:           http.ProxyURL(proxyUrl),
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	ChoTotClient.Transport = tr */

}

func GetListAds(c echo.Context) error {

	limitselect := c.QueryParam("limit")
	lm, _ := strconv.Atoi(limitselect)
	if lm < 1 {
		lm = 1
	}
	listAds, _ := mongo.FindAll(lm)

	if listAds == nil {
		return c.JSON(http.StatusNotFound, types.PayloadResponse("400", "List empty"))
	}
	return c.JSON(http.StatusOK, listAds)
}

func DetailAds(c echo.Context) error {

	var ChototListAds types.ChototDetailsAds

	id, _ := strconv.Atoi(c.Param("id"))

	LinkDetailAdsGet := fmt.Sprintf(LinkDetailAds, id)

	fmt.Println("LinkListAds:", LinkDetailAdsGet)

	rs, err := ChoTotClient.Get(LinkDetailAdsGet)
	// Process response
	if err != nil {
		log.Errorf("Wrong request %s", err)
	}
	if rs.StatusCode == 429 {
		fmt.Println("Limit API RATE:")
	}
	defer rs.Body.Close()
	bodyBytes, err := ioutil.ReadAll(rs.Body)
	if err != nil {
		return c.JSON(http.StatusBadRequest, types.PayloadResponse("wrong_request", fmt.Sprintf("%s", err)))
	}
	if err := json.Unmarshal(bodyBytes, &ChototListAds); err != nil {
		log.Errorf("Wrong bodyBytes %s", err)
		return c.JSON(http.StatusBadRequest, types.PayloadResponse("wrong_request", fmt.Sprintf("%s", err)))
	}
	return c.JSON(http.StatusOK, ChototListAds.Body)

}

func GetAllListAds(c echo.Context) error {

	c.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSONCharsetUTF8)
	c.Response().WriteHeader(http.StatusOK)

	var currentPage int

	ListCrawled, _ := mongo.FindLastOne()

	currentPage = ListCrawled.CurentPage

	ChototListAds := CrawlListAdsData(fmt.Sprintf(LinkListAds, currentPage))

	TotalPage := ChototListAds.Total / len(ChototListAds.Ads)

	for currentPage < TotalPage {
		//time.Sleep(time.Nanosecond * 10000)
		func() {
			var ChototListAds2Save *types.ChototListAds
			LinkListAds1 := fmt.Sprintf(LinkListAds, currentPage)
			ChototListAds2Save = CrawlListAdsData(LinkListAds1)
			fmt.Println("TotalPage:", TotalPage)
			fmt.Println("Ads:", len(ChototListAds2Save.Ads))
			if len(ChototListAds2Save.Ads) > 0 {
				currentPage++
			} else {
				time.Sleep(time.Second * 5)
				ChototListAds2Save = CrawlListAdsData(LinkListAds1)
			}
			if len(ChototListAds2Save.Ads) > 0 {
				ChototListAds2Save.CurentPage = currentPage + 1
				if errx := json.NewEncoder(c.Response()).Encode(ChototListAds2Save); errx != nil && len(ChototListAds2Save.Ads) < 1 {
					log.Errorf("ERR:", errx)
				}
				mongo.Insert(ChototListAds2Save)
				c.Response().Flush()
			}

		}()
	}
	fmt.Println("TotalPage:", TotalPage)

	return nil

	//return c.JSON(http.StatusOK, res)
}

func CrawlListAdsData(linkCrwal string) *types.ChototListAds {

	fmt.Println("LinkListAds:", linkCrwal)
	var ChototListAds types.ChototListAds
	rs, err := ChoTotClient.Get(linkCrwal)
	// Process response
	if err != nil {
		log.Errorf("Wrong request %s", err)
	}
	if rs.StatusCode == 429 {
		fmt.Println("Limit API RATE:")
	}
	fmt.Println("rs.StatusCode:", rs.StatusCode)
	defer rs.Body.Close()
	bodyBytes, err := ioutil.ReadAll(rs.Body)
	if err != nil {
		log.Errorf("Wrong request %s", err)
		panic(err)
	}
	if err := json.Unmarshal(bodyBytes, &ChototListAds); err != nil {
		log.Errorf("Wrong bodyBytes %s", err)
		//panic(err)
	}
	/*Not error will be json here*/
	return &ChototListAds
}
