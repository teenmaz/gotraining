package mongoDB

import (
	"log"
	"sync"
	"time"

	"g.ghn.vn/go-training/tientp/config"

	mgo "gopkg.in/mgo.v2"
)

var instance = make(map[string]*Connection)
var once sync.Once

func GetInstance(name string) *Connection {
	once.Do(func() {
		instance[name] = &Connection{
			name: name,
		}
	})
	return instance[name]
}

type Connection struct {
	session *mgo.Session
	err     bool
	name    string
}

func (c *Connection) GetSession() *mgo.Session {
	return c.session
}

func (c *Connection) GetCollection(name string) *mgo.Collection {
	DBName := config.Config.Mongo[c.name].Name
	return c.session.DB(DBName).C(name)
}

func (c *Connection) SetError(err bool) {
	c.err = err
}

func (c *Connection) GetError() bool {
	return c.err
}

func (c *Connection) ensureConnected() error {
	for {
		if c.GetError() {
			log.Printf("Trying to reconnect MongoDB")
			c.SetError(false)
			return c.Dial()
		}
		time.Sleep(2 * time.Second)
	}
}

func (c *Connection) Dial() error {
	session, err := mgo.Dial(config.Config.Mongo[c.name].Host)
	if err != nil {
		log.Println("Cant connect mongoDB")
	} else {
		c.session = session
		log.Println("mongoDB connected")
	}
	go c.ensureConnected()
	return err
}
