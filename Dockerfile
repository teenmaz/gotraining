FROM golang:1.8 as builder
WORKDIR /go/src/g.ghn.vn/go-training/tientp/
COPY . /go/src/g.ghn.vn/go-training/tientp
RUN go build -o ./dist/demo

FROM alpine:3.5
RUN apk add --update ca-certificates
RUN apk add --no-cache tzdata && \
  cp -f /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime && \
  apk del tzdata

FROM ubuntu:latest
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
RUN echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | tee /etc/apt/sources.list.d/10gen.list
RUN apt-get update
RUN apt-get install mongodb-10gen
RUN mkdir -p /data/db
RUN apt-get install -y influxdb && \
  service influxdb start

RUN apt-get install -y wget
RUN wget https://s3-us-west-2.amazonaws.com/grafana-releases/release/grafana_4.5.2_amd64.deb && \
  apt-get install -y adduser libfontconfig && \
  dpkg -i grafana_4.5.2_amd64.deb 

WORKDIR /tientp
COPY ./config/echodemo.yaml /var/app/
COPY ./config/echodemo.yaml /
COPY --from=builder go/src/g.ghn.vn/go-training/tientp/dist/demo .
EXPOSE 9090
ENTRYPOINT ["./demo"]
