package main

import (
	"net/http"
	"strconv"
	"sync"
	"time"

	"g.ghn.vn/go-common/profiler/echo.v3"
	. "g.ghn.vn/go-training/tientp/config"
	"g.ghn.vn/go-training/tientp/handlers"
	"g.ghn.vn/go-training/tientp/libs/mongoDB"
	"g.ghn.vn/go-training/tientp/logger"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	validatorxxx "gopkg.in/go-playground/validator.v9"

	"github.com/rcrowley/go-metrics"
	"github.com/vrischmann/go-metrics-influxdb"
)

var log = logger.GetLogger("Cache")

type CustomValidator struct {
	cvalidate *validatorxxx.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.cvalidate.Struct(i)
}

type (
	Stats struct {
		Uptime       time.Time      `json:"uptime"`
		RequestCount uint64         `json:"requestCount"`
		Statuses     map[string]int `json:"statuses"`
		mutex        sync.RWMutex
	}
)

func NewStats() *Stats {
	return &Stats{
		Uptime:   time.Now(),
		Statuses: map[string]int{},
	}
}

// Process is the middleware function.
func (s *Stats) Process(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if err := next(c); err != nil {
			c.Error(err)
		}
		s.mutex.Lock()
		defer s.mutex.Unlock()
		s.RequestCount++
		status := strconv.Itoa(c.Response().Status)
		s.Statuses[status]++
		return nil
	}
}

// Handle is the endpoint to get stats.
func (s *Stats) Handle(c echo.Context) error {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	return c.JSON(http.StatusOK, s)
}

// ServerHeader middleware adds a `Server` header to the response.
func ServerHeader(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Response().Header().Set(echo.HeaderServer, "Echo/3.0")
		return next(c)
	}
}

func main() {

	mongo := mongoDB.GetInstance("default")

	err := mongo.Dial()

	session := mongo.GetSession()

	defer session.Close()

	if err != nil {
		log.Error(err)
	}

	e := echo.New()

	e.Debug = true

	//-------------------
	// Custom middleware
	//-------------------
	// Stats
	s := NewStats()
	e.Use(s.Process)
	e.GET("/stats", s.Handle) // Endpoint to get stats

	// Server header
	e.Use(ServerHeader)

	e.Use(middleware.RecoverWithConfig(middleware.RecoverConfig{
		StackSize: 1 << 10, // 1 KB
	}))

	e.Validator = &CustomValidator{cvalidate: validatorxxx.New()}

	if Config.Profiler.StatsdAddress != "" {
		e.Use(profiler.ProfilerWithConfig(profiler.ProfilerConfig{Address: Config.Profiler.StatsdAddress, Service: Config.Profiler.Service}))
	}

	rChoTot := e.Group("/chotot")

	rChoTot.GET("/ads/crawl", handlers.GetAllListAds)

	rChoTot.GET("/ads/list", handlers.GetListAds)

	rChoTot.GET("/ads/:id", handlers.DetailAds)

	e.GET("/health", handlers.HealthCheck)

	rmess := metrics.NewRegistry()
	metrics.RegisterDebugGCStats(rmess)
	metrics.RegisterRuntimeMemStats(rmess)

	go metrics.CaptureDebugGCStats(rmess, time.Second*5)
	go metrics.CaptureRuntimeMemStats(rmess, time.Second*5)

	go influxdb.InfluxDB(
		rmess,                   // metrics registry
		time.Second*5,           // interval
		"http://localhost:8086", // the InfluxDB url
		"chotot",                // your InfluxDB database
		"",                      // your InfluxDB user
		"",                      // your InfluxDB password
	)

	/* e.POST("/api/v1/public/login", handlers.Login)
	e.POST("/api/v1/public/register", handlers.Register)
	r := e.Group("/api/v1/private")
	Use and config JWT
	r.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		Claims:     &types.JwtTPTClaims{},
		SigningKey: []byte(Config.Encryption.SecretKey),
	}))
	r.GET("/profile", handlers.GetPrivateProfile) */
	port := "9090"
	log.Info("Starting at port: " + port)
	err = e.Start(":" + port)
	if err != nil {
		log.Error(err)
	}

}
