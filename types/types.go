package types

import (
	jwt "github.com/dgrijalva/jwt-go"
	"gopkg.in/mgo.v2/bson"
)

type errorResponse struct {
	ErrorCode string `json:"err_code"`
	Message   string `json:"message"`
}

type OkStatus struct {
	Ok bool `json:"ok"`
}

type okResponse struct {
	Ok      bool   `json:"ok"`
	Message string `json:"message"`
}

func PayloadResponse(code string, message string) errorResponse {
	return errorResponse{
		ErrorCode: code,
		Message:   message,
	}
}

type Profile struct {
	AccountID  bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty" groups:"public,private,internal"`
	FullName   string        `json:"full_name" bson:"full_name" groups:"public,private,internal"`
	Phone      string        `json:"phone,omitempty" bson:"phone,omitempty" groups:"public,private,internal"`
	Email      string        `json:"email" bson:"email" groups:"public,private,internal"`
	AccountOID string        `json:"account_oid,omitempty" bson:"account_oid,omitempty" groups:"public,private,internal"`
	PassWord   string        `json:"password" bson:"password"`
	Token      string        `json:"token" bson:"token"`
}

type ProfileRequest struct {
	FullName *string `json:"full_name,omitempty" groups:"public" validate:"required"`
	Phone    *string `json:"phone,omitempty" bson:"phone,omitempty" groups:"public,private,internal" validate:"required"`
	Email    *string `json:"email,omitempty" groups:"public" validate:"required,email"`
	PassWord *string `json:"password" bson:"password" validate:"required"`
}

type LoginRequest struct {
	Email    *string `json:"email,omitempty" groups:"public" validate:"required,email"`
	PassWord *string `json:"password" bson:"password" groups:"public" validate:"required"`
}

type JwtTPTClaims struct {
	Sub       string `json:"sub"`
	Ttype     string `json:"type"`
	Pol       string `json:"pol"`
	Name      string `json:"full_name"`
	Email     string `json:"email"`
	AccountID string `json:"_id"`
	jwt.StandardClaims
}

/*Struct for data list ads*/
type BodyChototListAds struct {
	ListID   int    `json:"list_id" bson:"list_id,omitempty"`
	Subject  string `json:"subject" bson:"subject,omitempty"`
	Image    string `json:"image" bson:"image"`
	ListTime int64  `json:"list_time" bson:"list_time"`
}

type ChototListAds struct {
	ID         bson.ObjectId          `json:"_id" bson:"_id,omitempty"`
	Total      int                    `json:"total" bson:"total,omitempty"`
	Ads        []BodyChototDetailsAds `json:"ads" bson:"ads,omitempty"`
	CurentPage int
}

/*Struct Details of Ads*/
type BodyChototDetailsAds struct {
	ListID       int      `json:"list_id" bson:"list_id,omitempty"`
	Subject      string   `json:"subject,omitempty" bson:"subject,omitempty"`
	Images       []string `json:"images,omitempty" bson:"images,omitempty"`
	Thumbnail    string   `json:"image,omitempty" bson:"image,omitempty"`
	Body         string   `json:"body,omitempty" bson:"body,omitempty"`
	CategoryName string   `json:"category_name,omitempty" bson:"category_name,omitempty"`
	PriceString  string   `json:"price_string,omitempty" bson:"price_string,omitempty"`
	TimePost     int64    `json:"list_time,omitempty" bson:"list_time,omitempty"`
}

type ChototDetailsAds struct {
	Body BodyChototDetailsAds `json:"ad,omitempty"`
}
